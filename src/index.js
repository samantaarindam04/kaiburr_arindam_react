import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import 'primeicons/primeicons.css';
import { PrimeReactProvider } from 'primereact/api';
import 'primeflex/primeflex.css';
import 'primereact/resources/primereact.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import { Provider } from 'react-redux'
import { store } from './redux/store'
import Loading from './components/Loading';
import ErrorBoundaries from './ErrorBoundaries';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ErrorBoundaries fallback="There is some error">
      <Suspense fallback={<Loading />}>
        <Provider store={store}>
          <PrimeReactProvider>
            <App />
          </PrimeReactProvider>
        </Provider>
      </Suspense>
    </ErrorBoundaries>
  </React.StrictMode>
);
