
export const productUrl = (row, first) => {
    return `https://dummyjson.com/products?limit=${row}&select=title,brand,price,stock&skip=${first}`
}
export const searchUrl = (row, first, search) => {
    return `https://dummyjson.com/products/search?limit=${row}&skip=${first}&q=${search}`
}


