import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { addSelectedProduct, addState } from '../../redux/reducers/tableReducer';

export default function Table() {
    const [loading, setLoading] = useState(false);

    const lazyStates = useSelector((state) => state.lazystate.lazystate)
    const allProds = useSelector((state) => state.lazystate.allproducts)
    const selProds = useSelector((state) => state.lazystate.selectedproduct)
    const dispatch = useDispatch()

    const onPage = (event) => {
        dispatch(addState(event))
    };

    const onSelectionChange = (event) => {
        dispatch(addSelectedProduct(event.value))
    };

    useEffect(() => {
        setLoading(true);
        let networkTimeout = setTimeout(async () => {
            if (allProds.skip === 0 && allProds?.products?.length > 0) {
                const sProds = allProds.products.slice(0, 5)
                selProds.length === 0 && dispatch(addSelectedProduct(sProds))
            }
            setLoading(false);
        }, Math.random() * 1000 + 250);
        return (() => clearTimeout(networkTimeout))
    }, [ dispatch])

    return (
        <DataTable
            value={allProds.products}
            lazy
            dataKey="id"
            paginator
            first={lazyStates.first}
            rows={lazyStates.rows}
            totalRecords={allProds.total}
            onPage={onPage}
            loading={loading}
            tableStyle={{ minWidth: '100%' }}
            selection={selProds}
            onSelectionChange={onSelectionChange}
            globalFilterFields={['title', 'brand']}
            rowsPerPageOptions={[5, 10, 25, 50, 100]}
        >
            <Column selectionMode="multiple" headerStyle={{ width: '3rem' }} />
            <Column field="title" header="Title" />
            <Column field="brand" header="Brand" />
            <Column field="price" header="Price" />
            <Column field="stock" header="Stock" />
        </DataTable>
    )
}