import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { useCallback, useEffect, useState } from 'react';
import axios from 'axios'
import { InputText } from 'primereact/inputtext';
import Plot from 'react-plotly.js';

export default function Table() {

    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(false);
    const [totalRecords, setTotalRecords] = useState(0);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [selectedChartPrice, setSelectedChartPrice] = useState(null);
    const [selectedChartProduct, setSelectedChartProduct] = useState(null);
    const [globalFilterValue, setGlobalFilterValue] = useState('');

    const [lazyState, setlazyState] = useState({
        first: 0,
        rows: 10,
        page: 1,
        filters: {
            title: { value: '', matchMode: 'contains' },
            brand: { value: '', matchMode: 'contains' },
        },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        searchProduct(value)
        setGlobalFilterValue(value);
    };

    const fetchProduct = useCallback(async () => {
        const response = await axios.get(`https://dummyjson.com/products?limit=${lazyState.rows}&select=title,brand,price,stock&skip=${lazyState.first}`).then((resp) => resp.data)
        setTotalRecords(response.total)
        setProducts(response.products)
        const sProds = response.products.slice(0, 5)
        prepareChart(sProds)
        setSelectedProduct(sProds)
    },[lazyState])

    const searchProduct = async (search) => {
        const response = await axios.get(`https://dummyjson.com/products/search?limit=${lazyState.rows}&skip=${lazyState.first}&q=${search}`).then((resp) => resp.data)
        setTotalRecords(response.total)

        setProducts(response.products)
    }

    const renderHeader = () => {
        return (
            <div className="flex flex-wrap gap-2 justify-content-between align-items-center">
                <h4 className="m-0">Product Lists</h4>
                <span className="p-input-icon-left">
                    <i className="pi pi-search" />
                    <InputText value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Keyword Search" />
                </span>
            </div>
        );
    };

    const header = renderHeader();


    const onPage = (event) => {
        fetchProduct()
        setlazyState(event);
    };

    const prepareChart = (value) => {
        const product = value.map(key => key.title)
        const price = value.map(key => key.price)
        setSelectedChartPrice(price)
        setSelectedChartProduct(product)
    }

    const onSelectionChange = (event) => {
        const value = event.value;
        prepareChart(value)
        setSelectedProduct(value);
    };

    useEffect(() => {
        setLoading(true);
        let networkTimeout = setTimeout(async () => {
            fetchProduct()
            setLoading(false);
        }, Math.random() * 1000 + 250);
        return (() => clearTimeout(networkTimeout))
}, [lazyState, fetchProduct])

return (
    <div className='main'>
        <DataTable
            value={products}
            lazy
            header={header}
            dataKey="id"
            paginator
            first={lazyState.first}
            rows={lazyState.rows}
            totalRecords={totalRecords}
            onPage={onPage}
            loading={loading}
            tableStyle={{ minWidth: '100%' }}
            selection={selectedProduct}
            onSelectionChange={onSelectionChange}
            globalFilterFields={['title', 'brand']}
            rowsPerPageOptions={[5, 10, 25, 50, 100]}
        >
            <Column selectionMode="multiple" headerStyle={{ width: '3rem' }} />
            <Column field="title" header="Title" style={{ minWidth: '25%' }} />
            <Column field="brand" header="Brand" style={{ minWidth: '25%' }} />
            <Column field="price" header="Price" style={{ minWidth: '25%' }} />
            <Column field="stock" header="Stock" style={{ minWidth: '25%' }} />
        </DataTable>
        <div className='chart'>

            {
                selectedChartProduct && selectedChartProduct.length > 0 ? <Plot
                    data={[

                        { type: 'bar', x: selectedChartProduct, y: selectedChartPrice },
                    ]}
                    layout={{ width: '100%', height: 'auto', title: 'Price Virtualization' }}
                /> : 'No product selected'
            }
        </div>
    </div>
)
}