import { InputText } from 'primereact/inputtext'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addSearch } from '../../redux/reducers/tableReducer';

export default function TableSearchInput() {
    const globalFilterValue = useSelector((state)=>state.lazystate.searchterm)

    const dispatch = useDispatch()
    const onGlobalFilterChange = (e) => {
        dispatch(addSearch(e.target.value))
    }
    return (
        <div className="flex flex-wrap gap-2 justify-content-between align-items-center ">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText name="search" value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Keyword Search" />
            </span>
        </div>
    )
}
