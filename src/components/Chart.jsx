import React, { useEffect, useState } from 'react'
import Plot from 'react-plotly.js';
import { useSelector } from 'react-redux'

function Chart() {
    const [selectedChartPrice, setSelectedChartPrice] = useState(null);
    const [selectedChartProduct, setSelectedChartProduct] = useState(null);

    const allProducts = useSelector((state) => state.lazystate.selectedproduct)

    useEffect(() => {
        const product = allProducts.length > 0 && allProducts.map(key => key.title)
        const price = allProducts.length > 0 && allProducts.map(key => key.price)
        setSelectedChartPrice(price)
        setSelectedChartProduct(product)
    }, [allProducts])

    return (
        <div className='chart'>
            {
                selectedChartProduct && selectedChartProduct.length > 0 ? <Plot
                    data={[
                        { type: 'bar', x: selectedChartProduct, y: selectedChartPrice },
                    ]}
                    layout={{ width: '100%', height: 'auto', title: 'Price Virtualization' }}
                /> : 'No product selected'
            }
        </div>
    )
}

export default Chart