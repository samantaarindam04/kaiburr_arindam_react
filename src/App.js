import { useCallback, useEffect, useState } from 'react';
import './App.css';
import Chart from './components/Chart';
import Table from './components/Table/Table';
import axios from 'axios'
import { useSelector, useDispatch } from 'react-redux'
import { addProduct } from './redux/reducers/tableReducer';
import { productUrl, searchUrl } from './api/urls';
import Loading from './components/Loading';
import TableSearchInput from './components/Table/TableSearchInput';

function App() {
  const [url, setUrl] = useState('')
  const lazyStates = useSelector((state) => state.lazystate.lazystate)
  const search = useSelector((state) => state.lazystate.searchterm)
  const dispatch = useDispatch()
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)

  const fetch = useCallback(async (newUrl) => {
    if (newUrl === url) {
      try {
        setLoading(true)
        const response = await axios.get(url).then((resp) => resp.data)
        dispatch(addProduct(response))
      } catch (error) {
        setError(error)
      } finally {
        setLoading(false)
      }

    }
  }, [url, dispatch])

  const fetchProduct = useCallback(async () => {
    const newUrl = productUrl(lazyStates.rows, lazyStates.first)
    setUrl(newUrl)
    fetch(newUrl)
  }, [lazyStates, fetch])

  const searchProductByUrl = useCallback(async () => {
    console.log(search)
    const newUrl = searchUrl(lazyStates.rows, lazyStates.first, search)
    setUrl(newUrl)
    fetch(newUrl)
  }, [search, fetch, lazyStates])

  useEffect(() => {
    if (search) {
      searchProductByUrl()
    } else {
      fetchProduct()
    }
  }, [search, fetchProduct, searchProductByUrl, url])
  if (error) {
    return <div>Something went wrong</div>
  }
  return (
    <>
     <h2 className="m-0 header">Product Lists</h2>
      <TableSearchInput />
      {loading && <Loading />}
      {!loading &&
        <div className="main">
          <Table />
          <Chart />
        </div>
      }
    </>
  );
}

export default App;
