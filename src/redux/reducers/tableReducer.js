import { createSlice } from '@reduxjs/toolkit';

const tableSlice = createSlice({
    name: 'table',
    initialState: {
        allproducts: [],
        lazystate: {
            first: 0,
            rows: 10,
            page: 1,
            filters: {
                title: { value: '', matchMode: 'contains' },
                brand: { value: '', matchMode: 'contains' },
            },
        },
        selectedproduct: [],
        searchterm: ''
    },
    reducers: {
        addState: (state, action) => {
            state.lazystate = action.payload
        },
        addSelectedProduct: (state, action) => {
            state.selectedproduct = action.payload
        },
        addProduct: (state, action) => {
            state.allproducts = action.payload
        },
        addSearch: (state, action) => {
            state.searchterm = action.payload
        }
    }
})
export const { addState, addProduct, addSelectedProduct, addSearch } = tableSlice.actions
export default tableSlice.reducer;