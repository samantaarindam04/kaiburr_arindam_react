import { configureStore, combineReducers } from '@reduxjs/toolkit'
import tableReducer from './reducers/tableReducer'

const rootReducer = combineReducers({
    lazystate: tableReducer
})

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    })
})